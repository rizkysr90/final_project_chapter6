const express = require('express');
const app = express();
const myAppRouter = require('./src/routes/myApp.route');
const morgan = require('morgan');
const swaggerJSON = require('./swagger.json');
const swaggerUI = require('swagger-ui-express');

app.use('/docs',swaggerUI.serve,swaggerUI.setup(swaggerJSON));
app.use(morgan('tiny'));
app.use(express.json());
app.use(express.urlencoded({extended : true}));

app.use('/', myAppRouter);
app.all('*',(req,res) => {
    res.status(404).render('404_error_template', {title: "Sorry, page not found"});
});
/* Error handler middleware */
app.use((err, req, res, next) => {
    const statusCode = err.statusCode || 500;
    const statusMessage = err.message || 'Internal Server Error';
    res.status(statusCode).json({
        error : {
            message : statusMessage,
            code : statusCode,
            errors : err.errors
        }
    });
    
});

module.exports = app;