'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Orders', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      quantity: {
        type: Sequelize.INTEGER
      },
      customer_id: {
        type: Sequelize.INTEGER,
        references : {
          model : "Customers",
          key : "id"
        },
        onUpdate : 'CASCADE',
        onDelete : 'SET NULL'
      },
      product_id: {
        type: Sequelize.INTEGER,
        references : {
          model : "Products",
          key : "id"
        },
        onUpdate : 'CASCADE',
        onDelete : 'SET NULL'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Orders');
  }
};