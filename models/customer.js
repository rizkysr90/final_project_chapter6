'use strict';
const {
  Model
} = require('sequelize');
const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
  class Customer extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      const options = {
        foreignKey: 'role_id',
      }

      models.Role.hasMany(models.Customer,options);
      models.Customer.belongsTo(models.Role,options)
    }
  }
  Customer.init({
    email: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Customer',
    hooks: {
      beforeCreate: async (Customer, options) => {
        Customer.password = await bcrypt.hash(Customer.password, +process.env.SALT_HASH);
        return Customer;
      }
    }
  });
  return Customer;
};