'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Customer.hasMany(models.Order,{
        foreignKey: 'customer_id'
      });
      models.Order.belongsTo(models.Customer,{
        foreignKey: 'customer_id'
      });
      models.Product.hasMany(models.Order,{
        foreignKey: 'product_id'
      });
      models.Order.belongsTo(models.Product,{
        foreignKey: 'product_id'
      });

    }
  }
  Order.init({
    quantity: DataTypes.INTEGER,
    customer_id: DataTypes.INTEGER,
    product_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Order',
  });
  return Order;
};