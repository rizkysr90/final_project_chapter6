'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Profile extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Customer.hasOne(models.Profile,{
        foreignKey: 'customer_id'
      });
      models.Profile.belongsTo(models.Customer,{
        foreignKey: 'customer_id'
      });
    }
  }
  Profile.init({
    name: DataTypes.STRING,
    phone: DataTypes.STRING,
    address: DataTypes.STRING,
    customer_id: DataTypes.INTEGER,
    profile_picture: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Profile',
  });
  return Profile;
};