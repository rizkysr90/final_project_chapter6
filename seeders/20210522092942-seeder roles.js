'use strict';
const rolesData = require('../masterdata/order.json');
module.exports = {
  async up (queryInterface, Sequelize) {
    const mapped = rolesData.map((eachElm) => {
      eachElm.createdAt = new Date();
      eachElm.updatedAt = new Date();
      return eachElm;
    })
    await queryInterface.bulkInsert('Roles',mapped);
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Roles', null, {truncate:true,restartIdentity:true});
  }
};
