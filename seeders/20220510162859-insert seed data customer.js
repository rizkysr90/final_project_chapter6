'use strict';
const customerData = require('../masterdata/customer.json');
module.exports = {
  async up (queryInterface, Sequelize) {
    const mapped = customerData.map((eachElm) => {
      eachElm.createdAt = new Date();
      eachElm.updatedAt = new Date();
      eachElm.password =  bcrypt.hashSync(eachElm.password, +process.env.SALT_HASH)
      return eachElm;
    })
    await queryInterface.bulkInsert('Customers',mapped);
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Customers',null,{});
  }
};
