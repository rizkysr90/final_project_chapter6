'use strict';
const profileData = require('../masterdata/profile.json');
module.exports = {
  async up (queryInterface, Sequelize) {
    const mapped = profileData.map((eachElm) => {
      eachElm.createdAt = new Date();
      eachElm.updatedAt = new Date();
      return eachElm;
    })
    await queryInterface.bulkInsert('Profiles',mapped);
  },

  async down (queryInterface, Sequelize) {
  
    await queryInterface.bulkDelete('Customers', null, {});
  }
};
