'use strict';
const productData = require('../masterdata/product.json');
module.exports = {
  async up (queryInterface, Sequelize) {
    const mapped = productData.map((eachElm) => {
      eachElm.createdAt = new Date();
      eachElm.updatedAt = new Date();
      return eachElm;
    })
    await queryInterface.bulkInsert('Products',mapped);
  },

  async down (queryInterface, Sequelize) {
  
    await queryInterface.bulkDelete('Products', null, {});
  }
};
