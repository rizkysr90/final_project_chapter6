'use strict';
const orderData = require('../masterdata/roles.json');
module.exports = {
  async up (queryInterface, Sequelize) {
    const mapped = orderData.map((eachElm) => {
      eachElm.createdAt = new Date();
      eachElm.updatedAt = new Date();
      return eachElm;
    })
    await queryInterface.bulkInsert('Orders',mapped);
  },

  async down (queryInterface, Sequelize) {
  
    await queryInterface.bulkDelete('Orders', null, {});
  }
};
