'use strict';
const adminData = require('../masterdata/admin.json');
const bcrypt = require('bcrypt');
module.exports = {
  async up (queryInterface, Sequelize) {
    const mapped = adminData.map((eachElm) => {
      eachElm.createdAt = new Date();
      eachElm.updatedAt = new Date();
      eachElm.password =  bcrypt.hashSync(eachElm.password, +process.env.SALT_HASH)
      return eachElm;
    })
    await queryInterface.bulkInsert('Customers',mapped);
  },

  async down (queryInterface, Sequelize) {
    const whereClause = {
      email : "admin@gmail.com"
    }
    await queryInterface.bulkDelete('Customers', whereClause, {});
  }
};
