const customerService = require('../../services/customer/customer.service');
const {create : createProfileCustomerService,
    remove : removeProfileCustomerService} = require('../../services/profile/profile.service')
const baseResponse = require('../../models/baseResponse.model');

async function login(req,res,next) {
    try {
        const result = await customerService.login(req.body);
        res.status(200).json(baseResponse.statusOK(result))
    } catch (error) {
        next(error)
    }
}
async function getAll(req,res,next) {
    try {
        const result = await customerService.getAll(req.query);
        res.status(200).json(baseResponse.statusOK(result));
    } catch(err) {
        next(err);
    }
}
async function create(req,res,next) {
    try {
        const result = await customerService.create(req.body);
        await createProfileCustomerService(result)
        res.status(201).json(baseResponse.created(result));
    } catch (err) {
        next(err)
    }
}
async function update(req,res,next) {
    try {
        const result = await customerService.update(req.body,req.params.customer_id);
        res.status(200).json(baseResponse.statusOK(result));
    } catch (err) {
        next(err);
    }
}
async function getById(req,res,next) {
    try {
        const result = await customerService.getById(req.params.customer_id);
        res.status(200).json(baseResponse.statusOK(result));
    } catch (err) {
        next(err);
    }
}
async function remove(req,res,next) {
    try {
        await removeProfileCustomerService(req.params.customer_id);
        const result = await customerService.remove(req.params.customer_id);
        res.status(200).json(baseResponse.statusOK(result));
    } catch (err) {
        next(err);
    }
}

module.exports = {
    getAll,
    create,
    update,
    getById,
    remove,
    login
}