const orderService = require('../../services/order/order.service');
const baseResponse = require('../../models/baseResponse.model');
async function getAll(req,res,next) {
    try {
        const result = await orderService.getAll(req.query);
        res.status(200).json(baseResponse.statusOK(result));
    } catch(err) {
        next(err);
    }
}
async function create(req,res,next) {
    try {
        const result = await orderService.create(req.body);
        res.status(201).json(baseResponse.created(result));
    } catch(err) {
        next(err);
    }
}
async function update(req,res,next) {
    try {
        const result = await orderService.update(req.body,req.params.order_id);
        res.status(200).json(baseResponse.statusOK(result));
    } catch(err) {
        next(err);
    }
}
async function getById(req,res,next) {
    try {
        const result = await orderService.getById(req.params.order_id);
        res.status(200).json(baseResponse.statusOK(result));
    } catch (err) {
        next(err);
    }
}
async function remove(req,res,next) {
    try {
        const result = await orderService.remove(req.params.order_id);
        res.status(200).json(baseResponse.statusOK(result));
    } catch (err) {
        next(err);
    }
}

module.exports = {
    getAll,
    create,
    update,
    getById,
    remove
}