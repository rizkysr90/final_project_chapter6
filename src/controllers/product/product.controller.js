const productService = require('../../services/product/product.service');
const baseResponse = require('../../models/baseResponse.model');
async function getAll(req,res,next) {
    try {
        const result = await productService.getAll(req.query);
        res.status(200).json(baseResponse.statusOK(result));
    } catch(err) {
        next(err);
    }
}
async function create(req,res,next) {
    try {
        const result = await productService.create(req.body);
        res.status(201).json(baseResponse.created(result));
    } catch (err) {
        next(err)
    }
}
async function update(req,res,next) {
    try {
        const result = await productService.update(req.body,req.params.product_id);
        res.status(200).json(baseResponse.statusOK(result));
    } catch (err) {
        next(err);
    }
}
async function remove(req,res,next) {
    try {
        const result = await productService.remove(req.params.product_id);
        res.status(200).json(baseResponse.statusOK(result));
    } catch (err) {
        next(err);
    }
}
async function getById(req,res,next) {
    try {
        const result = await productService.getById(req.params.product_id);
        res.status(200).json(baseResponse.statusOK(result));
    } catch (err) {
        next(err);
    }
}


module.exports = {
    getAll,
    create,
    update,
    remove,
    getById
}