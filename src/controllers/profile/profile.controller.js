const profileService = require('../../services/profile/profile.service');
const baseResponse = require('../../models/baseResponse.model');
async function getAll(req,res,next) {
    try {
        const result = await profileService.getAll(req.query);
        res.status(200).json(baseResponse.statusOK(result));
    } catch(err){
        next(err);
    }
}
async function update(req,res,next) {
    try {
        const result = await profileService.update(req.params.customer_id,req.body);
        res.status(200).json(baseResponse.statusOK(result));
    } catch(err) {
        next(err);
    }
}
async function getById(req,res,next) {
    try {
        const result = await profileService.getById(req.params.customer_id);
        res.status(200).json(baseResponse.statusOK(result));
    } catch(err) {
        next(err);
    }
}
module.exports = {
    getAll,
    update,
    getById
}
// Finish