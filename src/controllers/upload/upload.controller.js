const uploadService = require('../../services/upload/upload.service');
const baseResponse = require('../../models/baseResponse.model');

async function profilePicture(req,res,next) {
    try {
        const result = await uploadService.update(req);
        res.status(200).json(baseResponse.statusOK(result))
    } catch (error) {
        next(error)
    }
}


module.exports = {
    profilePicture
}