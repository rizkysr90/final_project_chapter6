const bcrypt = require('bcrypt');
const jsonwebtoken = require('jsonwebtoken');

module.exports = {
    async verifyPassword(passwordFromReqBody,hash) {
        return bcrypt.compare(passwordFromReqBody,hash);
    },
    issueJWT(customer) {
        const id = customer.id;
        const roleId = customer.role_id;
        const expiresIn = '1 days';
        const payload = {
            sub: id,
            role:roleId,
            iat: Date.now()
        };
        const signedToken = jsonwebtoken.sign(payload, process.env.JWT_SECRET, { expiresIn: expiresIn });
        return {
            token : signedToken,
            expires: expiresIn
        }
    }
}
