const cloudinary = require('cloudinary').v2;
const fs = require('fs');
const BaseResponse = require('../models/baseResponse.model');

cloudinary.config({ 
    cloud_name: 'rizkysr90', 
    api_key: '916351991818561', 
    api_secret: 'WnpJEPsAM6AogGlOOlMWC5xZ2ps' 
});
const sendVideoToCloud = async (req, res, next) => {
    try {
        const foldering = `my-asset/video`;
        const uploadResult = await cloudinary.uploader.upload(req.file.path, {
            resource_type: "video",
            folder: foldering
        });
        fs.unlinkSync(req.file.path);
        res.status(201).json(BaseResponse.statusOK(uploadResult.secure_url));
    } catch (error) {
        fs.unlinkSync(req.file.path);
        next(error)
    }
};

module.exports = {
    sendVideoToCloud
}