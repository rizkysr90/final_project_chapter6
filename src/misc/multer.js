const multer = require('multer');
const fs = require('fs');
const ErrorResponse = require('../models/errorResponse.model');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const fileLocation = './public/static/images';
        if (!fs.existsSync(fileLocation)) fs.mkdirSync(fileLocation, { recursive: true });
        cb(null, fileLocation);
    },
    filename: (req, file, cb) => {
        const fileType = file.mimetype.split('/')[1];
        cb(null, `profile-picture-` + file.fieldname + '-' + Date.now() + `.${fileType}`);
    }
});

const uploadVideo = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        // The function should call `cb` with a boolean
        // to indicate if the file should be accepted
        const validType = ['video/mp4','video/3gp'];
        if (validType.includes(file.mimetype)) return cb(null, true);
        // To reject this file pass `false`, like so:
        cb(null, false);

        // You can always pass an error if something goes wrong:
        cb(new ErrorResponse.BadRequest({errors : 'Wrong file type,only accept mp4 \ 3gp '}));
    },
    limits: {
        fileSize: 8000000
    }
});
const uploadImage = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        // The function should call `cb` with a boolean
        // to indicate if the file should be accepted
        const validType = ['image/png','image/jpg','img/png'];
        if (validType.includes(file.mimetype)) return cb(null, true);
        // To reject this file pass `false`, like so:
        cb(null, false);

        // You can always pass an error if something goes wrong:
        cb(new ErrorResponse.BadRequest({errors : 'Wrong file type,only accept png \ jpeg \ jpg'}));
    },
    limits: {
        fileSize: 5000000
    }
});

module.exports = {
    uploadImage,
    uploadVideo
}
