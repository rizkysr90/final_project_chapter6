const passport = require('passport');
const {Customer} = require('../../models/');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

const options = {
    jwtFromRequest : ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey : process.env.JWT_SECRET,
};

const strategy = new JwtStrategy(options,async (payload,done) => {
    const foundUserOptions = {
        attributes: {exclude: ['password','createdAt','updatedAt']},
        where : {
            id : payload.sub
        }
    };
    try {
        const foundUser = await Customer.findOne(foundUserOptions)
        if(!foundUser) {
            return done(null,false);
        }
        return done(null,foundUser);
    } catch (error) {
        return done(error,false);
    }
})
passport.use(strategy);
module.exports = passport.authenticate('jwt',{session:false});
