class BadRequest extends Error {
    constructor({errors}) {
        super('The server cannot or will not process the request due to something that is perceived to be a client error');
        this.name = "400 Bad Request"
        this.statusCode = 400;
        this.errors = errors;
    }
}

class NotFound extends Error {
    constructor({errors}) {
        super('The server can not find the requested resource.');
        this.name = "404 Not Found"
        this.statusCode = 404;
        this.errors = errors;
    }
}

class Unauthorized extends Error {
    constructor({errors}) {
        super(`The server understands the request but refuses to authorize it.`);
        this.name = "403 Forbidden"
        this.statusCode = 403
        this.errors = errors;
    }
}

module.exports =  {
    BadRequest,
    NotFound,
    Unauthorized
}
