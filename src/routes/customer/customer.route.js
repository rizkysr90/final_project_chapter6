const express = require('express');
const router = express.Router();
const customerController = require('../../controllers/customer/customer.controller');
const paginationValidator = require('../../validators/pagination.validator');
const customerValidator = require('../../validators/customer.validator');
const jwt = require('../../misc/passport');
const authRoles = require('../../misc/verifyRole');
router.route('/')
    .get(jwt,authRoles.roleValidator(['Admin']),authRoles.verifyRole,paginationValidator,customerController.getAll)
    .post(jwt,authRoles.roleValidator(['Admin']),authRoles.verifyRole,customerValidator.create(),customerValidator.validate,customerController.create);
router.route('/:customer_id')
    .put(jwt,authRoles.roleValidator(['Member']),authRoles.verifyRole,customerValidator.idParam(),customerValidator.create(),customerValidator.validate,customerController.update)
    .get(jwt,customerValidator.idParam(),customerValidator.validate,customerController.getById)
    .delete(jwt,authRoles.roleValidator(['Member']),authRoles.verifyRole,customerValidator.idParam(),customerValidator.validate,customerController.remove);
module.exports = router;