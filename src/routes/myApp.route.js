const express = require('express');
const router = express.Router();
const customerRouter = require('./customer/customer.route');
const profileRouter = require('./profile/profile.router');
const orderRouter = require('./order/order.route');
const productRouter = require('./product/product.router');
const authMiddleware = require('./../misc/auth/auth.middleware');
const {login : loginController} = require('../controllers/customer/customer.controller')
const loginValidator = require('../validators/login.validator');
const uploadRouter = require('./upload/upload.router')

router.get('/',(req,res,next) => res.send('Hello World From Rizki SR'));
router.post('/customers/login',loginValidator.login(),loginValidator.validate,loginController);
router.use('/customers',customerRouter);
router.use('/profile',profileRouter);
router.use('/orders',orderRouter);
router.use('/products',productRouter);
router.use('/uploads/',uploadRouter)
module.exports = router;