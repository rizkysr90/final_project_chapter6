const express = require('express');
const router = express.Router();
const orderController = require('../../controllers/order/order.controller');
const paginationValidator = require('../../validators/pagination.validator');
const orderValidator = require('../../validators/order.validator');
const jwt = require('../../misc/passport');
const authRoles = require('../../misc/verifyRole');


router.route('/')
    .get(jwt,authRoles.roleValidator(['Admin']),authRoles.verifyRole,paginationValidator,orderController.getAll)
    .post(jwt,authRoles.roleValidator(['Member']),authRoles.verifyRole,orderValidator.create(),orderValidator.validate,orderController.create)
router.route('/:order_id')
    .put(jwt,authRoles.roleValidator(['Member']),authRoles.verifyRole,orderValidator.idParam(),orderValidator.create(),orderValidator.validate,orderController.update)
    .get(jwt,orderValidator.idParam(),orderValidator.validate,orderController.getById)
    .delete(jwt,orderValidator.idParam(),orderValidator.validate,orderController.remove);
module.exports = router;