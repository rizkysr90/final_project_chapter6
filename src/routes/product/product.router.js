const express = require('express');
const router = express.Router();
const productController = require('./../../controllers/product/product.controller');
const paginationValidator = require('../../validators/pagination.validator');
const productValidator = require('../../validators/product.validator');
const jwt = require('../../misc/passport');
const authRoles = require('../../misc/verifyRole');


router.route('/')
    .get(jwt,paginationValidator,productController.getAll)
    .post(jwt,authRoles.roleValidator(['Admin']),authRoles.verifyRole,productValidator.create(),productValidator.validate,productController.create)
router.route('/:product_id')
    .get(jwt,productValidator.idParam(),productValidator.validate,productController.getById)
    .put(jwt,authRoles.roleValidator(['Admin']),authRoles.verifyRole,productValidator.idParam(),productValidator.create(),productValidator.validate,productController.update)
    .delete(jwt,authRoles.roleValidator(['Admin']),authRoles.verifyRole,productValidator.idParam(),productValidator.validate,productController.remove);

module.exports = router