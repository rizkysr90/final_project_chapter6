const express = require('express');
const router = express.Router();
const profileController = require('./../../controllers/profile/profile.controller');
const paginationValidator = require('../../validators/pagination.validator');
const profileValidator = require('../../validators/profile.validator');
const jwt = require('../../misc/passport');
const authRoles = require('../../misc/verifyRole');

router.route('/')
    .get(jwt,authRoles.roleValidator(['Admin']),authRoles.verifyRole,paginationValidator,profileController.getAll)
router.route('/:customer_id')
    .put(jwt,authRoles.roleValidator(['Member']),authRoles.verifyRole,profileValidator.idParam(),profileValidator.update(),profileValidator.validate,profileController.update)
    .get(jwt,profileValidator.idParam(),profileValidator.validate,profileController.getById)

module.exports = router;
