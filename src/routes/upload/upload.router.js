const router = require('express').Router();
const jwt = require('../../misc/passport');
const {uploadImage, uploadVideo} = require('../../misc/multer');
const uploadController = require('../../controllers/upload/upload.controller');
const {sendVideoToCloud} = require('../../misc/cloudinary');

router.post('/profile_picture',jwt,uploadImage.single('profile_picture'),uploadController.profilePicture)

router.post('/video',jwt,uploadVideo.single('video'),sendVideoToCloud);

module.exports = router;
