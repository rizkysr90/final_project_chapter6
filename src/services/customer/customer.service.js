// const db = require('./../db/db.service');
const {Customer,Profile} = require('../../../models');
const {Op} = require("sequelize");
const pagination = require('../../utils/pagination.util');
const apiError = require('../../models/errorResponse.model');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {verifyPassword,issueJWT} = require('../../libs/utils');


async function login(reqBody) {
    const { email, password } = reqBody;
    
    const foundUser = await Customer.findOne({
        
        where: {
            email: email
        }
    });
    if (!foundUser) {
        throw new apiError.NotFound({errors:'User not found'});
    }
    const isValidPassword = await verifyPassword(password, foundUser.password);
    if (isValidPassword) {
        return issueJWT(foundUser);
    }  
    throw new apiError.BadRequest({errors : 'Failed To Authenticate The Input'});
}
async function getAll(req) {
    const {page,row} = pagination(req.page,req.row);
    const options = {
        attributes: {exclude: ['password','createdAt','updatedAt']},
        
        limit : row,
        offset : page,
        order : [['id','ASC']]
    }
    return await Customer.findAll(options);
    
}
async function create(reqBody) {
    const checkUniqeEmail = await Customer.findOne({
        where : {
            "email" : reqBody.email
        }
    })
    if(checkUniqeEmail) {
        throw new apiError.BadRequest({errors : `Customer email  must be unique, email ${reqBody.email} was used`});
    }
    const createdUser = await Customer.create(reqBody);
    return {id : createdUser.id};
}
async function update(reqBody,customerId) {
    const checkAnyUser = await Customer.findOne({
        where : {
            id : customerId
        }
    })
    if(!checkAnyUser) {
        throw new apiError.NotFound({errors : 'User not found'});
    }
    const checkUniqeEmail = await Customer.findOne({
        where : {
            "email" : reqBody.email,
            "id" : {
                [Op.not] : customerId
            }
        }
    })
    if(checkUniqeEmail) {
        throw new apiError.BadRequest({errors : `Customer email  must be unique, email ${reqBody.email} was used`});
    }
    const result =  await Customer.update(reqBody,{
        where : {
            id : customerId
        }
    });
    if (result[0] === 0) {
        throw new apiError.NotFound({errors : `Customer with id ${customerId} not found`})
    }
    return result;
}
async function getById(customerId) {
    const result = await Customer.findOne({
        attributes: {exclude: ['password','createdAt','updatedAt','role_id']},
        where : {
            id : customerId
        },
        include : Profile
    });
    if(!result){
        throw new apiError.NotFound({errors : `Customer with id ${customerId} not found`})
    }
    return result;

}
async function remove(customerId) {
    const result = await Customer.destroy({where : {
        id : customerId
    }});
    if(result === 0){
        throw new apiError.NotFound({errors : `Customer with id ${customerId} not found`})
    }
    return result;
}
module.exports = {
    getAll,
    create,
    update,
    getById,
    remove,
    login
}
