const db = require('./../db/db.service');
const orderValidator = require('./../../validators/order/order.validator');
const {Customer,Product,Order} = require('../../../models');
const {Op} = require("sequelize");
const pagination = require('../../utils/pagination.util');
const apiError = require('../../models/errorResponse.model');

async function getAll(req) {

    const {page,row} = pagination(req.page,req.row);
    const options = {
        attributes : {
            exclude : ['createdAt','updatedAt']
        },
        limit : row,
        offset : page,
        order : [['id','ASC']],
        include: [Product,Customer]
    }

    return await Order.findAll(options);
}
async function create(reqBody) {
    const findProduct = await Product.findOne({
        where : {
            id : reqBody.product_id
    }})
    if (!findProduct) {
        throw new apiError.NotFound({errors : 'Product not found'})
    }
    const findUser = await Customer.findOne({
        where : {
            id : reqBody.customer_id
        }
    })
    if (!findUser) {
        throw new apiError.NotFound({errors : 'User not found'})
    }
    return await Order.create(reqBody).then((result) => result.id);
}
async function update(reqBody,orderId) {
    const findProduct = await Product.findOne({
        where : {
            id : reqBody.product_id
    }})
    if (!findProduct) {
        throw new apiError.NotFound({errors : 'Product not found'})
    }
    const findUser = await Customer.findOne({
        where : {
            id : reqBody.customer_id
        }
    })
    if (!findUser) {
        throw new apiError.NotFound({errors : 'User not found'})
    }
    const result =  await Order.update(reqBody,{
        where : {
            id : orderId
        }
    });
    if (result[0] === 0) {
        throw new apiError.NotFound({errors : `Order with id ${orderId} not found`})
    }
    return result;
}
async function getById(orderId) {

    const result = await Order.findOne({
        attributes: {exclude: ['customer_id','product_id']},

        where : {
            id : orderId
        },
        include: [Product,Customer]
    });
    if(!result){
        throw new apiError.NotFound({errors : `Order with id ${orderId} not found`})
    }
    return result;

}
async function remove(orderId) {
    const result = await Order.destroy({where : {
        id : orderId
    }});
    if(result === 0){
        throw new apiError.NotFound({errors : `Order with id ${orderId} not found`})
    }
    return result;
}
module.exports = {
    getAll,
    create,
    update,
    getById,
    remove
}