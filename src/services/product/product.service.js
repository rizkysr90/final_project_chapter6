const db = require('./../db/db.service');
const productValidator = require('./../../validators/product.validator');
const {Product} = require('../../../models/');
const {Op} = require("sequelize");
const pagination = require('../../utils/pagination.util');
const apiError = require('../../models/errorResponse.model');

async function getAll(req) {
    const {page,row} = pagination(req.page,req.row);
    const options = {
        attributes : {
            exclude : ['createdAt','updatedAt']
        },
        limit : row,
        offset : page,
        order : [['id','ASC']]
    }

    return await Product.findAll(options);
}
async function create(reqBody) {
    return await Product.create(reqBody);
}
async function update(reqBody,productId) {
    const result =  await Product.update(reqBody,{
        where : {
            id : productId
        }
    });
    if (result[0] === 0) {
        throw new apiError.NotFound({errors : `Product with id ${productId} not found`})
    }
    return result;
}
async function remove(productId) {
    const result = await Product.destroy({where : {
        id : productId
    }});
    if(result === 0){
        throw new apiError.NotFound({errors : `Product with id ${productId} not found`})
    }
    return result;
}
async function getById(productId) {
    const result = await Product.findOne({
        where : {
            id : productId
        }
    });
    if(!result){
        throw new apiError.NotFound({errors : `Product with id ${productId} not found`})
    }
    return result;
}
module.exports = {
    getAll,
    create,
    update,
    remove,
    getById
}