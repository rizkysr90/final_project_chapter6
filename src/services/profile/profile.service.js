const db = require('../db/db.service');
const pagination = require('../../utils/pagination.util');
const apiError = require('../../models/errorResponse.model');
const {Profile, Customer} = require('../../../models');

async function remove(customerId) {
    return await Profile.destroy({where : {
        "customer_id" : customerId
    }});
}
async function create({id : customerId}) {
    const preDefinedData = {
        name : null,
        phone : null,
        address : null,
        profile_picture: null,
        customer_id : customerId
    }

    const checkUniqueId = await Profile.findOne({
        where : {
            "customer_id" : customerId
        }
    })
    if(checkUniqueId) {
        throw new apiError.BadRequest({errors : `Customer id must be unique, customer with id ${data.code} was used to make some profile`});
    }
    return await Profile.create(preDefinedData);


}
async function getAll(req) {
    const {page,row} = pagination(req.page,req.row);
    const options = {
        attributes : {
            exclude : ['createdAt','updatedAt']
        },
        limit : row,
        offset : page,
        order : [['id','ASC']],
        include : Customer
    }
    return await Profile.findAll(options);
}
async function update(customerId,reqBody) {
    // Make an SQL Query For Updating Data
    const result = await Profile.update(reqBody,{
        where : {
            "customer_id" : customerId
        }
    })
    if(result[0] === 0){
        throw new apiError.NotFound({errors : `Customer with id ${customerId} not found`})
    }
    return result;
}
async function getById(customerId) {
    const result = await Profile.findOne({
        where : {
            "customer_id" : customerId
        },
        include : Customer
    });
    if(!result){
        throw new apiError.NotFound({errors : `Customer with id ${customerId} not found`})
    }
    return result;
}
module.exports = {
    getAll,
    update,
    getById,
    create,
    remove
}