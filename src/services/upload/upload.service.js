const apiError = require('../../models/errorResponse.model');
const {Profile} = require('../../../models');

async function update(req) {
    // console.log(req)
    const {customer_id} = req.body;
    const data = {
        profile_picture : req.file.path
    }
    const result = await Profile.update(data,{
        where : {
            "customer_id" : +customer_id
        }
    })
    if(result[0] === 0){
        throw new apiError.NotFound({errors : `Customer with id ${customer_id} not found`})
    }
    return result;
}

module.exports = {
    update
}