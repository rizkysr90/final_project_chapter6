const { body, param, query, validationResult } = require('express-validator');
const apiError = require('../models/errorResponse.model');
const constant = require('../constant/constant');

const create = () => {
    return [
        body(['customer_id','product_id','quantity'],constant.IS_EMPTY_MESSAGE).notEmpty(),
        body(['customer_id','product_id','quantity']).toInt()

    ]
}
const idParam = () => {
    return [
      param('order_id',constant.IS_NUMBER_MESSAGE).isInt()
    ]
  }
  
const validate = (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
          if (errors.errors[0].msg === constant.IS_EMPTY_MESSAGE ||
              errors.errors[0].msg === constant.IS_NUMBER_MESSAGE ||
              errors.errors[0].msg === constant.IS_INVALID_EMAIL) {
              throw new apiError.BadRequest({errors : errors.array()});
          }
      }
      next();
    } catch (error) {
        next(error)
    }
  }
  

module.exports = {
    create,
    validate,
    idParam
}