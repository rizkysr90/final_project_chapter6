const pagination = (req,res,next) => {
    if (req.query.page !== undefined) {
      req.query.page = Number(req.query.page);
      if (!req.query.page) {
        req.query.page = 1
      }
    }
    if (req.query.row !== undefined) {
      req.query.row = Number(req.query.row);
      if (!req.query.row) {
        req.query.row = 5
      }
    }
    next();
}

module.exports = pagination
