const customerService = require('../src/services/customer/customer.service');
const productService = require('../src/services/product/product.service');
const profileService = require('../src/services/profile/profile.service');

const { sequelize } = require('../models');
const app = require('../index');
const request = require('supertest');
const admin = require('../src/config/admin/admin.config.json');

const customerData = require('../masterdata/customer.json').map((eachData) => {
    eachData.createdAt = new Date();
    eachData.updatedAt = new Date();
    return eachData;
  })
const productData = require('../masterdata/product.json').map((eachData) => {
    eachData.createdAt = new Date();
    eachData.updatedAt = new Date();
    return eachData;
})
const profileData = require('../masterdata/profile.json').map((eachData) => {
    eachData.createdAt = new Date();
    eachData.updatedAt = new Date();
    return eachData;
})

beforeAll(async () => {
    try {
        await sequelize.sync({force:true});
        await sequelize.queryInterface.bulkInsert('Customers', customerData, {});
        await sequelize.queryInterface.bulkInsert('Products', productData, {});
        await sequelize.queryInterface.bulkInsert('Profiles', profileData, {});

    } catch(e) {
        console.log('Error at customer service test',e)
    }
        
},10000)

afterAll(async () => {    
    await sequelize.close();
})

describe('Test Service Customer', () => {
    test('SUCCESS,pagination test,GET /customers', async () => {
        const req = {
            page : 1,
            row : 1
            // maximum value of row is must be equal to customerData.length
        }
        const result = await customerService.getAll(req);
        expect(result.length).toBe(req.row);
    })
    test('FAILED,Customer id not found,GET /customers/:customer_id,',async () => {
        const customerId = 10000;
        try {
            await customerService.getById(customerId)

        } catch(e) {
            expect(e.message).toBe(`The server can not find the requested resource.`);
        }
    })
    test('SUCCESS,Create user,POST /customers',async () => {
        const data = {
            "email" : "user4@gmail.com",
            "password" : "12345"
        }
        const responseBody = {"id": 4}
        const response = await customerService.create(data);
        expect(response).toEqual(responseBody.id);
    });
    test('FAILED,Create user failed because email not unique,POST /customers',async () => {
        const data = {
            "email" : "user4@gmail.com",
            "password" : "12345"
        }
        try {
            await customerService.create(data)
        } catch(e) {
            expect(e.errors).toBe(`Customer email  must be unique, email ${data.email} was used`);
        }
    });
    test('FAILED,Create user failed because email is invalid ,POST /customers',async () => {
        const data = {
            "email" : "user100gmail.com",
            "password" : "12345678"
        }
        const response = await request(app)
            .post('/customers')
            .set('username',`${admin.username}`)
            .set('password',`${admin.password}`)
            .send(data)
        expect(response.status).toBe(400);
       
    })
})
describe('Test Service product', () => {
    test('SUCCESS,pagination test,GET /products', async () => {
        const req = {
            page : 1,
            row : 1
            // maximum value of row is must be equal to productData.length
        }
        const result = await productService.getAll(req);
        expect(result.length).toBe(req.row);
    })
    test('FAILED,product id not found,GET /products/:product_id,',async () => {
        const productId = 10000;
        try {
            await productService.getById(productId)

        } catch(e) {
            expect(e.message).toBe(`The server can not find the requested resource.`);
        }
    })
    test('SUCCESS,Create product,POST /products',async () => {
        const data ={
            "name" : "KFC Chicken Spicy",
            "price" : 20000,
        }
        const responseBody = {
            "id" : 4,
            "name" : "KFC Chicken Spicy",
            "price" : 20000,
        }
        const response = await productService.create(data).then((data) => {
            delete data.dataValues.createdAt
            delete data.dataValues.updatedAt

            return data.dataValues;
        });
        expect(response).toEqual(responseBody);
    });
    test('FAILED,Remove product failed because product_id not found,REMOVE /products',async () => {
        // const data ={
        //     "name" : "Nasi Jamblang Daging Semur",
        //     "price" : 'Dua rebu',
        // }
        const response = await request(app)
            .delete('/products/1000')
            .set('username',`${admin.username}`)
            .set('password',`${admin.password}`)
        expect(response.status).toBe(404);
    });
    test('FAILED,Create product failed because empty field,POST /products',async () => {
        const data ={
            "name" : "",
            "price" : 20000,
        }
        const response = await request(app)
            .post('/products')
            .set('username',`${admin.username}`)
            .set('password',`${admin.password}`)
            .send(data)
        expect(response.status).toBe(400);
       
    })
})
describe('Test Service Profile', () => {
    test('SUCCESS,pagination test,GET /profile', async () => {
        const req = {
            page : 1,
            row : 1
            // maximum value of row is must be equal to productData.length
        }
        const result = await profileService.getAll(req);
        expect(result.length).toBe(req.row);
    })
    test('FAILED,profile id not found,GET /profile/:profile_id,',async () => {
        const profileId = 10000;
        try {
            await profileService.getById(profileId)

        } catch(e) {
            expect(e.message).toBe(`The server can not find the requested resource.`);
        }
    })
    test('SUCCESS,Update product,PUT /profile/:customer_id',async () => {
        const data ={
            "name" : "Tsubatsa Ozora",
            "phone": "546464646",
            "address": "Jayakarta"
        }
        const response = await request(app)
            .put('/profile/1')
            .set('username',`${admin.username}`)
            .set('password',`${admin.password}`)
            .send(data)
        expect(response.status).toBe(200);
    });
    test('FAILED,Authenticate because email or password are wrong,GET /profile/:customer_id',async () => {

        const response = await request(app)
            .get('/profile/1000')
            .set('username',`wrong`)
            .set('password',`${admin.password}`)
        expect(response.status).toBe(403);
    });
    test('FAILED,Update product failed because empty field,PUT /profile/:customer_id',async () => {
        const data ={
            "name" : "",
            "phone": "546464646",
            "address": "Jayakarta"
        }
        const response = await request(app)
            .put('/profile/1')
            .set('username',`${admin.username}`)
            .set('password',`${admin.password}`)
            .send(data)
        expect(response.status).toBe(400);
    });
})

